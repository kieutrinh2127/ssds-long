package com.fpt.ssds.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "lookup")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class Lookup extends AbstractAuditingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "lookup_key")
    private String lookupKey;

    @Column(name = "lookup_value")
    private String lookupValue;

    @Column(name = "display_val")
    private String displayVal;

    @Column(name = "description")
    private String description;
}
