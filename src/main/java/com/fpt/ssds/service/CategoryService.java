package com.fpt.ssds.service;

import com.fpt.ssds.service.dto.CategoryDto;

public interface CategoryService {
    void createUpdate(CategoryDto categoryDto);
}
