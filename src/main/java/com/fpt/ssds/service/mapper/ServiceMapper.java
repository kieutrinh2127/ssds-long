package com.fpt.ssds.service.mapper;


import com.fpt.ssds.domain.Service;
import com.fpt.ssds.service.dto.ServiceDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link Service} and its DTO {@link ServiceDto}.
 */
@Mapper(componentModel = "spring", uses = {CategoryMapper.class, EquipmentTypeMapper.class})
public interface ServiceMapper extends EntityMapper<ServiceDto, Service> {
    @Mapping(source = "category.id", target = "categoryId")
    @Mapping(source = "equipmentType.id", target = "equipmentTypeId")
    ServiceDto toDto(Service location);

    @Mapping(source = "categoryId", target = "category")
    @Mapping(source = "equipmentTypeId", target = "equipmentType")
    Service toEntity(ServiceDto locationDTO);

    default Service fromId(Long id) {
        if (id == null) {
            return null;
        }
        Service service = new Service();
        service.setId(id);
        return service;
    }
}
