package com.fpt.ssds.service.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * A DTO for the {@link com.fpt.ssds.domain.ServiceType} entity
 */
@Data
public class ServiceTypeDto implements Serializable {
    private final Long id;
    private final String name;
    private final String code;
    private final String description;
}
