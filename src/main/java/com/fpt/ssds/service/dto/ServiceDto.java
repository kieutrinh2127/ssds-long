package com.fpt.ssds.service.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * A DTO for the {@link com.fpt.ssds.domain.Service} entity
 */
@Data
public class ServiceDto implements Serializable {
    private Long id;
    private String name;
    private String code;
    private String description;
    private ServiceTypeDto type;
    private Long duration;
    private Boolean isActive;
    private String categoryId;
    private String equipmentTypeId;
}
