package com.fpt.ssds.service.dto;

import com.fpt.ssds.domain.EquipmentType;
import lombok.Data;

import java.io.Serializable;
import java.time.Instant;

/**
 * A DTO for the {@link EquipmentType} entity
 */
@Data
public class EquipmentTypeDto implements Serializable {
    private String createdBy;
    private Instant createdDate;
    private String lastModifiedBy;
    private Instant lastModifiedDate;
    private Long id;
    private String name;
    private String code;
    private String description;
}
