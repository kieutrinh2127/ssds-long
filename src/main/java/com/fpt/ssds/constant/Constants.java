package com.fpt.ssds.constant;

/**
 * Application constants.
 */
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^(?>[a-zA-Z0-9!$&*+=?^_`{|}~.-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*)|(?>[_.@A-Za-z0-9-]+)$";

    public static final String SYSTEM = "system";
    public static final String DEFAULT_LANGUAGE = "en";

    private Constants() {}

    public class COMMON_TYPE {
        public static final String SERVICE_CATEGORY = "serviceCategory";
    }

    public class COMMON {
        public static final String REQUEST_ID = "ATT-REQUEST-ID";
    }
}
